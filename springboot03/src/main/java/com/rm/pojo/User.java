package com.rm.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

/**
 * @Author: Reggie
 * @Date: 2022/6/6 0:35
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    private Integer uid;
    private String uname;
    private String password;
}
