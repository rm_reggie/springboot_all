package com.rm.mapper;

import com.rm.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022/6/6 0:38
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
public interface UserMapper {
    List<User> findAllUser();
}
