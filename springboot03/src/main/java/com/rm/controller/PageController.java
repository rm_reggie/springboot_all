package com.rm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: Reggie
 * @Date: 2022-06-07 21:24
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Controller
public class PageController {

    @RequestMapping("testPages/{url}")
    public String toPage(@PathVariable("url") String url){
        return url;
    }
}
