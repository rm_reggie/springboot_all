package com.rm.controller;

import com.github.pagehelper.PageInfo;
import com.rm.pojo.Emp;
import com.rm.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022-06-07 0:07
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Controller
public class EmpController {

    @Autowired
    private EmpService empService;

    @ResponseBody
    @RequestMapping("findAllEmp")
    public List<Emp> findAllEmp(){
        return empService.findAllEmp();
    }

    @ResponseBody
    @RequestMapping("findByPage/{pageNum}/{pageSize}")
    public PageInfo<Emp> findByPage(@PathVariable("pageNum") Integer pageNum, @PathVariable("pageSize") Integer pageSize){
        return empService.findByPage(pageNum,pageSize);
    }
}
