package com.rm.controller;

import com.rm.pojo.User;
import com.rm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022/6/6 0:36
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping("findAllUser")
    public List<User> findAllUser(){
        return userService.findAllUser();
    }
}
