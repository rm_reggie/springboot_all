package com.rm.service.impl;

import com.rm.mapper.UserMapper;
import com.rm.pojo.User;
import com.rm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022/6/6 0:38
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAllUser() {
        return userMapper.findAllUser();
    }
}
