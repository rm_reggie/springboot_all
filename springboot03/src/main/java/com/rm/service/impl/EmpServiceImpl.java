package com.rm.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rm.mapper.EmpMapper;
import com.rm.pojo.Emp;
import com.rm.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022-06-07 0:08
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Service
public class EmpServiceImpl implements EmpService {
    @Autowired
    private EmpMapper empMapper;

    @Override
    public List<Emp> findAllEmp() {
        return empMapper.findAllEmp();
    }

    @Override
    public PageInfo<Emp> findByPage(Integer pageNum, Integer pageSize) {
        /*Page<Emp> page = */PageHelper.startPage(pageNum, pageSize);
        List<Emp> list =  empMapper.findAllEmp();
        //System.out.println("当前页："+page.getPageNum());
        //System.out.println("总页数："+page.getPages());
        //System.out.println("页大小："+page.getPageSize());
        //System.out.println("总记录："+page.getTotal());
        //System.out.println("当前页："+page.getResult());
        //List<Emp> list1 = empMapper.findAllEmp(); // PageInfo<>();
        return new PageInfo<Emp>(list);
    }
}
