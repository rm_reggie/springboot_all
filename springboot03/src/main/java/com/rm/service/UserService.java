package com.rm.service;

import com.rm.pojo.User;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022/6/6 0:37
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
public interface UserService {
    List<User> findAllUser();
}
