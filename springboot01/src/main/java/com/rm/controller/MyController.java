package com.rm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: Reggie
 * @Date: 2022/6/5 14:27
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Controller
public class MyController {

    @ResponseBody
    @RequestMapping("firstController")
    public String firstController(){
        return "hello springboot";
    }
}
