package com.rm.controller;

import com.rm.pojo.Emp;
import com.rm.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * @Author: Reggie
 * @Date: 2022-06-08 0:38
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Controller
public class ThymeleafController {

    @Autowired
    private EmpService empService;

    @RequestMapping("showIndex")
    public String showIndex(Map<String, Object> map) {
        map.put("msg", "testMessage");
        return "index";
    }

    @RequestMapping("showEmp")
    public String showEmp(Map<String, Object> map) {
        List<Emp> empList = empService.findAll();
        empList.forEach(System.out::println);
        map.put("emp", empList.get(0));
        map.put("empList", empList);
        return "showEmp";
    }
}
