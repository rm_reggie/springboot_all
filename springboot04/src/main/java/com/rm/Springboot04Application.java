package com.rm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Reggie
 * @Date: 2022-06-07 22:11
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@SpringBootApplication
@MapperScan("com.rm.mapper")
public class Springboot04Application {
    public static void main(String[] args) {
        SpringApplication.run(Springboot04Application.class,args);
    }
}
