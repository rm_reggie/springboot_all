package com.rm.service;

import com.rm.pojo.Emp;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022-06-15 22:27
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
public interface EmpService {
    List<Emp> findAll();
}
