package com.rm.service.impl;

import com.rm.mapper.EmpsMapper;
import com.rm.pojo.Emp;
import com.rm.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022-06-15 22:28
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Service
public class EmpServiceImpl implements EmpService {
    @Autowired
    private EmpsMapper empsMapper;

    @Override
    public List<Emp> findAll() {
        return empsMapper.findAll();
    }
}
