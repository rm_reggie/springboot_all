package com.rm.service;

import com.rm.pojo.Users;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022-06-06 23:21
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
public interface UsersService {
    List<Users> findAll();
}
