package com.rm.service.impl;

import com.rm.mapper.UsersMapper;
import com.rm.pojo.Users;
import com.rm.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022-06-06 23:23
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Service
public class UserServiceImpl implements UsersService {

    @Autowired
    private UsersMapper usersMapper;
    @Override
    public List<Users> findAll() {
        return usersMapper.findAll();
    }
}
