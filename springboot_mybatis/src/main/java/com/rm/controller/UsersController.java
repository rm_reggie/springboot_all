package com.rm.controller;

import com.rm.pojo.Users;
import com.rm.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022-06-06 23:09
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Controller
public class UsersController {

    @Autowired
    private UsersService usersService;

    @ResponseBody
    @RequestMapping("test")
    public String test(){
        return "test";
    }

    @ResponseBody
    @RequestMapping("findAll")
    public List<Users> findAll(){
        return usersService.findAll();
    }
}
