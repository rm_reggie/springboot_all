package com.rm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Reggie
 * @Date: 2022-06-06 23:01
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@SpringBootApplication
public class SpringMyBatisApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringMyBatisApplication.class,args);
    }
}
