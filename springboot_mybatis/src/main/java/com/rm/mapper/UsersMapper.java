package com.rm.mapper;

import com.rm.pojo.Users;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: Reggie
 * @Date: 2022-06-06 23:17
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Mapper
public interface UsersMapper {
    List<Users> findAll();
}
