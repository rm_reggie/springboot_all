package com.rm.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Reggie
 * @Date: 2022-06-06 23:16
 * @Version: 1.0
 * 如果你想复制粘贴代码的时候，就应该考虑重构了。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Users implements Serializable {
    private Integer uid;
    private String uname;
    private String password;
}